package com.gaoxi.gaoxi;

import com.alibaba.dubbo.config.annotation.Service;
/**
 * UserServiceImpl
 *
 * @Author: JXL
 * @Date: 2021/12/14 14:02
 **/
@Service(version = "1.0.0")
@org.springframework.stereotype.Service
public class UserServiceImpl implements UserService {

	@Override
	public String login(String loginReq) {
		return "res:"+loginReq;
	}
}