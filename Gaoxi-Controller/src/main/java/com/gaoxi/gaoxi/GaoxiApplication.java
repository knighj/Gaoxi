package com.gaoxi.gaoxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class GaoxiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GaoxiApplication.class, args);
    }

}
