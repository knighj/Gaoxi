package com.gaoxi.gaoxi;

import com.alibaba.dubbo.config.annotation.Reference;
import com.gaoxi.rsp.Result;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * UserControllerImpl
 *
 * @Author: JXL
 * @Date: 2021/12/14 14:07
 **/
@RestController
public class UserControllerImpl {

	@Reference(version = "1.0.0")
	private UserService userService;

	public Result login(String loginReq, HttpServletResponse httpRsp) {
		// 登录鉴权
		String userEntity = userService.login(loginReq);

		// 登录成功
		return Result.newSuccessResult(userEntity);
	}
}